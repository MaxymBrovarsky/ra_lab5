import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RiskComponent } from './risk/risk.component';
import { RiskOriginComponent } from './risk-origin/risk-origin.component';
import { RiskProbabilityComponent } from './risk-probability/risk-probability.component';
import {RiskCategoryService} from './service/riskcategory.service';
import {Risk} from './model/risk.model';
import {RiskCategoryRepository} from './repository/riskcaterogy.repository';
import {RiskCategoryDataSource} from './datasource/riskcategory.datasource';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RiskOriginCategoryRepository} from './repository/riskorigincategory.repository';
import {RiskOrigin} from './model/riskorigin.model';
import {RiskOriginCategoryDataSource} from './datasource/riskorigincategory.datasource';
import {RiskOriginCategoryService} from './service/riskorigincategory.sevice';
import { PossibleLossesComponent } from './possible-losses/possible-losses.component';
import { EventComponent } from './event/event.component';
import {EventDataSource} from './datasource/event.datasource';
import { RiskMonitoringComponent } from './risk-monitoring/risk-monitoring.component';
import { StorageServiceModule } from 'ngx-webstorage-service';
import {LocalStorageService} from './service/localstorage.service';

@NgModule({
  declarations: [
    AppComponent,
    RiskComponent,
    RiskOriginComponent,
    RiskProbabilityComponent,
    PossibleLossesComponent,
    EventComponent,
    RiskMonitoringComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    StorageServiceModule
  ],
  providers: [
    RiskCategoryService,
    RiskCategoryRepository,
    RiskCategoryDataSource,
    RiskOriginCategoryService,
    RiskOriginCategoryRepository,
    RiskOriginCategoryDataSource,
    EventDataSource,
    LocalStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
