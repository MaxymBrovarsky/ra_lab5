import {Injectable} from '@angular/core';
import {RiskOrigin} from '../model/riskorigin.model';
import {RiskOriginCategory} from '../model/riskorigincategory.model';

@Injectable()
export class RiskOriginCategoryDataSource {
  private static data: RiskOriginCategory[] = new Array<RiskOriginCategory>(
    new RiskOriginCategory('Множина настання технічних ризикових подій',
      [
        new RiskOrigin('функціональні характеристики ПЗ', 1),
        new RiskOrigin('характеристики якості ПЗ', 1),
        new RiskOrigin('характеристики надійності ПЗ', 1),
        new RiskOrigin('застосовність ПЗ', 1),
        new RiskOrigin('часова продуктивність ПЗ', 1),
        new RiskOrigin('супроводжуваність ПЗ', 1),
        new RiskOrigin('повторне використання компонент ПЗ', 1)
      ]),
    new RiskOriginCategory('Множина настання вартісних ризикових подій',
      [
        new RiskOrigin('обмеження сумарного бюджету на програмний проект', 1),
        new RiskOrigin('недоступна вартість реалізації програмного проекту', 1),
        new RiskOrigin('низька ступінь реалізму при оцінюванні витрат на програмний проект', 1)
      ]),
    new RiskOriginCategory('Множина джерел появи планових ризиків',
      [
        new RiskOrigin('властивості та можливості гнучкості внесення змін до планів життєвого циклу розроблення ПЗ', 1),
        new RiskOrigin('можливості порушення встановлених термінів реалізації етапів життєвого циклу розроблення ПЗ', 1),
        new RiskOrigin('низька ступінь реалізму при встановленні планів і етапів життєвого циклу розроблення ПЗ', 1)
      ]),
    new RiskOriginCategory('Множина джерел появи ризиків реалізації процесу управління програмним проектом',
      [
        new RiskOrigin('хибна стратегія реалізації програмного проекту', 1),
        new RiskOrigin('неефективне планування проекту розроблення ПЗ', 1),
        new RiskOrigin('неякісне оцінювання програмного проекту', 1),
        new RiskOrigin('прогалини в документуванні етапів реалізації програмного проекту', 1),
        new RiskOrigin('промахи в прогнозуванні результатів реалізації програмного проекту', 1)
      ])
  );

  constructor() {

  }

  static getData() {
    return this.data;
  }
}
