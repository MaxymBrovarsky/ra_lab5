import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventDataSource} from '../datasource/event.datasource';
import {RiskCategoryDataSource} from '../datasource/riskcategory.datasource';
import {Risk} from '../model/risk.model';
import {Event} from '../model/event.model';
import {LocalStorageService} from '../service/localstorage.service';
import {RiskCategory} from '../model/riskcategory.model';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit, OnDestroy {
  riskCategories: RiskCategory[] = [];
  constructor(private eventsDataSource: EventDataSource,
              private risksDataSource: RiskCategoryDataSource,
              private localStorage: LocalStorageService ) {

  }

  public getRisks(): Risk[] {
    let risks = [];
    for (let c of this.riskCategories) {
      risks.push(...c.risks);
    }
    return risks;
  }

  public getEvents(): Event[] {
    return this.eventsDataSource.getData();
  }

  ngOnInit() {
    this.riskCategories = this.localStorage.load();
  }

  ngOnDestroy(): void {
    this.localStorage.save(this.riskCategories);
  }


}
