import { Component, OnInit } from '@angular/core';
import {RiskCategoryService} from '../service/riskcategory.service';
import {RiskCategory} from '../model/riskcategory.model';

@Component({
  selector: 'app-risk',
  templateUrl: './risk.component.html',
  styleUrls: ['./risk.component.css']
})
export class RiskComponent implements OnInit {
  riskCategories: RiskCategory[];
  constructor(private service: RiskCategoryService) {
    // this.riskCategories = service.getRiskCategories();
  }

  get totalSize():number {
    return this.riskCategories.reduce((tv, c2) =>  tv + c2.categorySize, 0);
  }


  getRiskCategories(): void{
    this.riskCategories = this.service.getRiskCategories();
  }
  ngOnInit() {
    this.getRiskCategories();
  }

}
