import {Component, OnDestroy, OnInit} from '@angular/core';
import {RiskCategory} from '../model/riskcategory.model';
import {RiskCategoryService} from '../service/riskcategory.service';
import {LocalStorageService} from '../service/localstorage.service';

@Component({
  selector: 'app-risk-probability',
  templateUrl: './risk-probability.component.html',
  styleUrls: ['./risk-probability.component.css']
})
export class RiskProbabilityComponent implements OnInit, OnDestroy {
  riskCategories: RiskCategory[];
  hardcode = 0.1;
  static expertsCoefficient: number[] = [];
  constructor(private riskCategoryService: RiskCategoryService, private localStorage: LocalStorageService) {
    // for (let i = 0; i < 10; i++) {
    //   RiskProbabilityComponent.expertsCoefficient[i] = this.hardcode + Math.random() * 0.7;
    // }
    // for (let i = 0 ; i < this.riskCategories.length; i++) {
    //   this.riskCategories[i].setExpertProbability(RiskProbabilityComponent.expertsCoefficient);
    //   this.riskCategories[i].setRiskExpertProbability(false);
    // }
    // this.riskCategoryService.setRiskCategories(this.riskCategories);
  }

  get totalSize():number {
    return this.riskCategories.reduce((tv, c2) =>  tv + c2.categorySize, 0);
  }

  getRiskCategories(): void{
    this.riskCategories = this.riskCategoryService.getRiskCategories();
  }

  public probabilityToString(p: number):string {
    if (p < 0.1) {
      return "Дуже низькою";
    } else if (p < 0.25) {
      return "Низькою";
    } else if (p < 0.5) {
      return "Середньою";
    } else if (p < 0.75) {
      return "Високою";
    } else {
      return "Дуже високою";
    }
  }


  ngOnInit() {
    console.log("qqq" + this.localStorage.load());
    if (this.localStorage.load().length == 0) {
      console.log('default load');
      this.getRiskCategories();
      for (let i = 0; i < 10; i++) {
        RiskProbabilityComponent.expertsCoefficient[i] = this.hardcode + Math.random() * 0.7;
      }
      for (let i = 0 ; i < this.riskCategories.length; i++) {
        this.riskCategories[i].setExpertProbability(RiskProbabilityComponent.expertsCoefficient);
        this.riskCategories[i].setRiskExpertProbability(false);
      }
      this.riskCategoryService.setRiskCategories(this.riskCategories);
      this.localStorage.save(this.riskCategories);
    } else {
      console.log('loading in component');
      this.riskCategories = this.localStorage.load();
    }

  }

  ngOnDestroy() {
    this.riskCategoryService.setRiskCategories(this.riskCategories);
    this.localStorage.save(this.riskCategories);
  }

}
