import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskProbabilityComponent } from './risk-probability.component';

describe('RiskProbabilityComponent', () => {
  let component: RiskProbabilityComponent;
  let fixture: ComponentFixture<RiskProbabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskProbabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskProbabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
