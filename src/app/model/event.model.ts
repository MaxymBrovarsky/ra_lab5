export class Event {
  title: string;
  value: number;


  public constructor(title: string, value: number) {
    this.title = title;
    this.value = value;

  }
}
