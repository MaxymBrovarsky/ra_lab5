export class Risk {
  title: string;
  value: number;
  probability: number;
  expertsProbability: number[];
  expertsProbabilityForLosses: number[];
  expertsProbabilityForLossesWithValidity: number[];
  expertsProbabilityWithValidity: number[];
  avgExpertsProbability: number;
  price: number;
  avgPrice: number;
  avgExpertsProbabilityWithValidity: number;
  additionalPrice: number;
  finishPrice: number;
  eventValue: number = 0;

  public constructor(title?: string, value?: number,state?:Risk) {
    this.title = title;
    this.value = value;
    if (state) {
      console.log('setting state');
      this.title = state.title;
      this.expertsProbability = state.expertsProbability;
      this.eventValue = state.eventValue;
      this.additionalPrice= state.additionalPrice;
      this.finishPrice = state.finishPrice;
      this.expertsProbabilityForLossesWithValidity = state.expertsProbabilityForLossesWithValidity;
      this.value = state.value;
      this.expertsProbabilityForLosses = state.expertsProbabilityForLossesWithValidity;
      this.avgPrice = state.avgPrice;
      this.probability = state.probability;
      this.avgExpertsProbabilityWithValidity = state.avgExpertsProbabilityWithValidity;
      this.expertsProbabilityWithValidity = state.expertsProbabilityWithValidity;
      this.avgExpertsProbability = state.avgExpertsProbability;
      this.price = state.price;
    }
  }


  public setProbability(value: number): void {
    this.probability = value;
  }

  public setExpertsProbability(a: number[], expertValidity: number[], expertValiditySum: number) {
    this.expertsProbability = a;
    this.avgExpertsProbability =a.reduce((t ,v) => t + v, 0) / a.length;
    if (this.value > 0) {
      this.expertsProbabilityWithValidity = new Array(10);
      let sum = 0;
      for (let i = 0; i < a.length; i++) {
        this.expertsProbabilityWithValidity[i] = a[i] * expertValidity[i];
        sum += a[i] * expertValidity[i];
      }
      this.avgExpertsProbabilityWithValidity = sum / expertValiditySum;
    }
  }

  public setExpertsProbabilityForLosses(a: number[], expertValidity: number[], sum: number): void {
    this.expertsProbabilityForLosses = a;
    this.setAveragePrice();
    this.expertsProbabilityForLossesWithValidity = [];
    for (let i = 0; i < expertValidity.length; ++i) {
      this.expertsProbabilityForLossesWithValidity[i] =
        this.expertsProbabilityForLosses[i] + Math.random() * expertValidity[i];
    }
    this.additionalPrice = this.getSumOfExpertsProbabilitiesWithValidityForLosses() / sum * this.price;
    this.finishPrice = this.additionalPrice + this.price;
  }

  public setAveragePrice(): void {
    this.avgPrice =
      this.expertsProbabilityForLosses.reduce((t, v) => t + v, 0) /
      this.expertsProbabilityForLosses.length *
      this.price;
  }

  private getSumOfExpertsProbabilitiesWithValidityForLosses(): number {
    return this.expertsProbabilityForLossesWithValidity.reduce((t, v) => t + v, 0);
  }

  public setEventValue(v: number) {
    console.log(v);
    this.eventValue = v;
  }


}
