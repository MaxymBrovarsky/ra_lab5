import {Risk} from './risk.model';

export class RiskCategory {
  title: string;
  expertProbability:number[];
  risks: Risk[] = [];
  expertValidity: number[] = [];
  avgProbabilitiesWithExpertValidity: number[] = [];
  expertValidityForLosses: number[] = [];
  avgValidityForLosses: number[] = [];
  avgProbability: number;
  probability: number;
  randomCoefficient: number;
  randomCoefficient2: number;
  price: number;


  public constructor(title?:string, risks?: Risk[], probability?: number, c?: RiskCategory) {
    if (c) {
      this.randomCoefficient2 = c.randomCoefficient2;
      let risks = [];
      for (let j = 0; j < c.risks.length; j++) {
        risks[j] = new Risk(null, null, c.risks[j]);
      }
      this.risks = risks;
      this.expertProbability = c.expertProbability;
      this.expertValidity = c.expertValidity;
      this.title = c.title;
      this.price = c.price;
      this.avgValidityForLosses = c.avgValidityForLosses;
      this.expertValidityForLosses = c.expertValidityForLosses;
      this.probability = c.probability;
      this.randomCoefficient = c.randomCoefficient;
      this.avgProbability = c.avgProbability;
      this.avgProbabilitiesWithExpertValidity = c.avgProbabilitiesWithExpertValidity;
    } else {

      console.log('qqqq');
      this.title = title;
      this.risks = risks;
      for (let i = 0; i < 10; ++i) {
        this.expertValidity[i] = Math.floor(Math.random() * 10) + 1;
      }
      this.probability = probability;
      for (let risk of risks) {
        risk.probability = this.probability + Math.random() * this.probability;
      }
      for (let i = 0; i < 10; ++i) {
        this.expertValidityForLosses[i] = Math.floor(Math.random() * 10) + 1;
      }
    }
  }

  addRisk(risk: Risk) {
    this.risks.push(risk);
  }

  get minExpertValidity():number {
    return Math.min(...this.expertValidity);
  }
  get categorySize():number {
    return this.risks.map(r => r.value).reduce((v1, v2) => v1+ v2, 0);
  }

  public setRandomCoefficient(v: number): void {
    this.randomCoefficient = v;
  }

  get expertValiditySum():number {
    return this.expertValidity.reduce((t, v) => t + v, 0);
  }

  get expertValidityForLossesSum():number {
    return this.expertValidityForLosses.reduce((t, v) => t + v, 0);
  }

  public riskProbability(i: number): number {
    return this.probability + Math.random() * this.probability;
  }

  public getCategoryCoefficient(): number {
    return this.categorySize / 100;
  }

  public getRiskPrice(i: number): number {
    this.risks[i].price = this.price *this.risks[i].probability / this.getRisksProbabilitySum();
    return this.risks[i].price;
  }

  private getRisksProbabilitySum(): number {
    return this.risks.reduce((t, v) => t + v.probability, 0);
  }

  public setExpertProbability(expertProbability:number[]): void {
    this.expertProbability = expertProbability;
  }

  public setRiskExpertProbability(useEvents: boolean): void {
    // console.log(this);
    if (!useEvents) {
      for (let risk of this.risks) {
        let ep = new Array(10);
        for (let i = 0; i < 10; i++) {
          ep[i] = risk.probability + Math.random() * this.expertProbability[i];
        }
        risk.setExpertsProbability(ep, this.expertValidity, this.expertValiditySum);
      }
      for (let i = 0; i < this.expertValidity.length; i++) {
        let sum = 0;
        let count = 0;
        for (let risk of this.risks) {
          if (risk.value > 0) {
            sum += risk.expertsProbability[i];
            count++;
          }
        }
        this.avgProbabilitiesWithExpertValidity[i] = sum / count / this.expertValidity[i];
      }
      let sum = 0;
      let count = 0;
      for (let risk of this.risks) {
        if (risk.value > 0) {
          sum += risk.avgExpertsProbabilityWithValidity;
          count++;
        }
      }
      this.avgProbability = sum / count;
    } else {
      for (let r of this.risks) {
        for(let i = 0; i< r.expertsProbability.length; i++) {
          console.log(r.expertsProbability[i]);
          r.expertsProbability[i] -= r.eventValue;
        }
        r.setExpertsProbability(r.expertsProbability, this.expertValidity, this.expertValiditySum);
      }
    }
  }

  public setRiskExpertProbabilityForLosses(expertProbability: number[]): void {
    for (let risk of this.risks) {
      let ep = [];
      for (let i = 0; i < 10; i++) {
        ep[i] = risk.probability + Math.random() * expertProbability[i];
      }
      risk.setExpertsProbabilityForLosses(ep, this.expertValidity, this.expertValidityForLossesSum);
      console.log('hui1');

    }
    for (let i = 0; i < this.expertValidity.length; i++) {
      console.log('hu2');
      let sum = 0;
      let count = 0;
      for (let risk of this.risks) {
        if (risk.value > 0) {
          console.log(risk.expertsProbabilityForLossesWithValidity);
          sum += risk.expertsProbabilityForLossesWithValidity[i];
          count++;
        }
      }
      this.avgValidityForLosses[i] = sum / count / this.expertValidity[i];
    }
  }

  public getAdditionalPrice(): number {
    return this.risks.reduce((t, v) => t + v.additionalPrice, 0);
  }

  public getTotalPrice(): number {
    return this.getAdditionalPrice() + this.price;
  }

  public getMinPrice(): number {
    return Math.min(...this.risks.map(r => r.additionalPrice));
  }

  public getMaxPrice(): number {
    return Math.max(...this.risks.map(r => r.additionalPrice));
  }






}
