import {RiskOrigin} from './riskorigin.model';

export class RiskOriginCategory {
  title: string;
  risks: RiskOrigin[] = [];
  public constructor(title?:string, risks?: RiskOrigin[]) {
    this.title = title;
    this.risks = risks;

  }

  addRiskOrigin(riskOrigin: RiskOrigin) {
    this.risks.push(riskOrigin);
  }
  get categorySize():number {
    return this.risks.map(r => r.value).reduce((v1, v2) => v1+ v2, 0);
  }
}
