import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import {RiskCategory} from '../model/riskcategory.model';
import {Risk} from '../model/risk.model';
// key that is used to access the data in local storageconst STORAGE_KEY = 'local_todolist';
const STORAGE_KEY = 'categories';

@Injectable()
export class LocalStorageService {
  riskCategories = [];
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {

  }

  public save(c: RiskCategory[]): void {
    console.log('saving');
    console.log(c);
    this.storage.set(STORAGE_KEY, JSON.stringify(c));
  }

  public load(): RiskCategory[] {
    let tmp = this.storage.get(STORAGE_KEY);
    if (tmp == undefined) {
      return [];
    }
    let rc = JSON.parse(this.storage.get(STORAGE_KEY));
    let t:RiskCategory[] = [];
    for (let i = 0; i < rc.length; i++) {
      let c = rc[i];
      console.log(c);
      let risks = [];
      for (let j = 0; j < c.risks.length; j++) {
        risks[j] = new Risk(null, null, c.risks[j]);
      }
      t.push(new RiskCategory(null, null, null,c));

    }
    return t;
  }
}
