import {Injectable} from '@angular/core';
import {RiskCategoryRepository} from '../repository/riskcaterogy.repository';
import {RiskCategory} from '../model/riskcategory.model';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RiskCategoryService {

  public constructor(private riskCategoryRepository: RiskCategoryRepository) {

  }
  public getRiskCategories():RiskCategory[] {
    return this.riskCategoryRepository.riskCategories;
  }

  getSumOfRiskRandomCoefficient() {
    return this.riskCategoryRepository.sumRandomCoefficients();
  }

  public setRiskCategories(rc: RiskCategory[]) {
    this.riskCategoryRepository.riskCategories = rc;
  }

}
