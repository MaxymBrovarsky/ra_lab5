import {Injectable} from '@angular/core';
import {RiskOriginCategoryRepository} from '../repository/riskorigincategory.repository';

@Injectable()
export class RiskOriginCategoryService {
  public constructor(private riskOriginCategoryRepository: RiskOriginCategoryRepository) {

  }
  getRiskOriginCategories() {
    return this.riskOriginCategoryRepository.riskOriginCategories;
  }

}
