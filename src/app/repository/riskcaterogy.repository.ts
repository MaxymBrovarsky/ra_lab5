import {Injectable} from '@angular/core';
import {RiskCategoryDataSource} from '../datasource/riskcategory.datasource';
import {RiskCategory} from '../model/riskcategory.model';

@Injectable({
  providedIn: 'root'
})
export class RiskCategoryRepository {
  riskCategories: RiskCategory[];
  constructor() {
    this.riskCategories = RiskCategoryDataSource.getData();
  }

  public sumRandomCoefficients(): number {
    return this.riskCategories.reduce((t, v) => t + v.randomCoefficient, 0);
  }



}
