import {Injectable} from '@angular/core';
import {RiskOriginCategory} from '../model/riskorigincategory.model';
import {RiskOriginCategoryDataSource} from '../datasource/riskorigincategory.datasource';

@Injectable()
export class RiskOriginCategoryRepository {
  riskOriginCategories: RiskOriginCategory[];
  constructor() {
    this.riskOriginCategories = RiskOriginCategoryDataSource.getData();
  }
}
