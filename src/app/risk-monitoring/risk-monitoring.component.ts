import { Component, OnInit } from '@angular/core';
import {RiskCategory} from '../model/riskcategory.model';
import {RiskCategoryService} from '../service/riskcategory.service';
import {RiskProbabilityComponent} from '../risk-probability/risk-probability.component';
import {LocalStorageService} from '../service/localstorage.service';

@Component({
  selector: 'app-risk-monitoring',
  templateUrl: './risk-monitoring.component.html',
  styleUrls: ['./risk-monitoring.component.css']
})
export class RiskMonitoringComponent implements OnInit {
  riskCategories: RiskCategory[];
  hardcode = 0.1;
  expertsCoefficient: number[];
  constructor(private riskCategoryService: RiskCategoryService, private localStorage: LocalStorageService) {
    // console.log("1");
    // this.riskCategories = riskCategoryService.getRiskCategories();
    // for (let i = 0 ; i < this.riskCategories.length; i++) {
    //   // this.riskCategories[i].setExpertProbability(RiskProbabilityComponent.expertsCoefficient);
    //   this.riskCategories[i].setRiskExpertProbability(false);
    // }
  }

  getRiskCategories(): void{
    this.riskCategories = this.riskCategoryService.getRiskCategories();
  }

  get totalSize():number {
    return this.riskCategories.reduce((tv, c2) =>  tv + c2.categorySize, 0);
  }

  public probabilityToString(p: number):string {
    if (p < 0.1) {
      return "Дуже низькою";
    } else if (p < 0.25) {
      return "Низькою";
    } else if (p < 0.5) {
      return "Середньою";
    } else if (p < 0.75) {
      return "Високою";
    } else {
      return "Дуже високою";
    }
  }
  ngOnInit() {
    console.log("2");
    this.riskCategories = this.localStorage.load();
    for (let i = 0 ; i < this.riskCategories.length; i++) {
      // this.riskCategories[i].setExpertProbability(RiskProbabilityComponent.expertsCoefficient);
      this.riskCategories[i].setRiskExpertProbability(true);
    }
    // this.riskCategoryService.setRiskCategories(this.riskCategories);
    // console.log(this.riskCategories);
  }

  ngOnDestroy() {
    console.log("3");
    console.log(this.riskCategories);

    this.riskCategoryService.setRiskCategories(this.riskCategories);
    this.localStorage.save(this.riskCategories);
  }

}
