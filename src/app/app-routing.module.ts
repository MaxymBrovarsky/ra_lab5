import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RiskOriginComponent} from './risk-origin/risk-origin.component';
import {RiskComponent} from './risk/risk.component';
import {RiskProbabilityComponent} from './risk-probability/risk-probability.component';
import {PossibleLossesComponent} from './possible-losses/possible-losses.component';
import {EventComponent} from './event/event.component';
import {RiskMonitoringComponent} from './risk-monitoring/risk-monitoring.component';


const routes: Routes = [
  { path: 'step1', component: RiskComponent},
  { path: 'step2', component: RiskOriginComponent},
  { path: 'step3', component: RiskProbabilityComponent},
  { path: 'step4', component: PossibleLossesComponent},
  { path: 'step5', component: EventComponent},
  { path: 'step6', component: RiskMonitoringComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
