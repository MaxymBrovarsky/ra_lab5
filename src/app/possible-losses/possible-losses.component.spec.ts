import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossibleLossesComponent } from './possible-losses.component';

describe('PossibleLossesComponent', () => {
  let component: PossibleLossesComponent;
  let fixture: ComponentFixture<PossibleLossesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossibleLossesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossibleLossesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
