import { Component, OnInit } from '@angular/core';
import {RiskCategoryService} from '../service/riskcategory.service';
import {RiskCategory} from '../model/riskcategory.model';
import {LocalStorageService} from '../service/localstorage.service';

@Component({
  selector: 'app-possible-losses',
  templateUrl: './possible-losses.component.html',
  styleUrls: ['./possible-losses.component.css']
})
export class PossibleLossesComponent implements OnInit {
  expertsCoefficient: number[] = [];
  hardcode = 0.2;
  intervals: number[] = [];
  riskCategories: RiskCategory[];

  constructor(private riskCategoryService: RiskCategoryService, private localStorage: LocalStorageService) {
    for (let i = 0; i < 10; i++) {
      this.expertsCoefficient[i] = this.hardcode + Math.random() * 0.5;
    }
  }

  getRiskCategories(): void{
    this.riskCategories = this.riskCategoryService.getRiskCategories();
  }

  ngOnInit() {
    this.getRiskCategories();
    this.setRandomCoefficients();
    this.setRandomCoefficients2();
    this.setCategoriesPrice();
    this.setRisksProbabilityForLosses();
    this.setIntervals();
  }

  private setRandomCoefficients(): void {
    let riskCategories = this.riskCategories;
    for (let i = 0; i < riskCategories.length; ++i) {
      let category = riskCategories[i];
      if (i + 1 < riskCategories.length) {
        category.setRandomCoefficient(category.categorySize + Math.random() * riskCategories[i + 1].categorySize);
      } else {
        category.setRandomCoefficient(category.categorySize);
      }
    }
  }
  private setRandomCoefficients2(): void {
    let riskCategories = this.riskCategories;
    for (let i = 0; i < riskCategories.length; ++i) {
      let category = riskCategories[i];
      category.randomCoefficient2 = category.randomCoefficient / this.riskCategoryService.getSumOfRiskRandomCoefficient();
    }
  }

  private setCategoriesPrice(): void {
    let tp = 300 + Math.random() * 1000;
    tp = Math.ceil(tp + Math.random()*tp/2);
    let riskCategories = this.riskCategories;
    for (let category of riskCategories) {
      category.price = tp * category.randomCoefficient2;
      for (let i = 0; i < category.risks.length; ++i) {
        category.getRiskPrice(i);
      }
    }
  }

  private setRisksProbabilityForLosses() {
    for (let c of this.riskCategories) {
      c.setRiskExpertProbabilityForLosses(this.expertsCoefficient);
    }
  }

  private setIntervals(): void {
    let min = 1000000000;
    let max = 0;
    for (let c of this.riskCategories) {
      if (c.getMinPrice() < min) {
        min = c.getMinPrice();
      }
      if (c.getMaxPrice() > max) {
        max = c.getMaxPrice();
      }
    }
    let mpr = (max - min) / 3;
    for (let i = 0; i < 4; ++i) {
      this.intervals[i] = min + i * mpr;
      console.log(this.intervals[i]);
    }
  }

  public priceToLabel(price: number): string {
    if (price >= this.intervals[0] && price < this.intervals[1]) {
      return "Низький";
    } else if (price >= this.intervals[1] && price < this.intervals[2]) {
      return "Середній";
    } else if (price >= this.intervals[2] && price <= this.intervals[3]) {
      return "Високий";
    } else {
      return "Виходить за межі";
    }
  }



}
