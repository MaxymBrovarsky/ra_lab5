import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskOriginComponent } from './risk-origin.component';

describe('RiskOriginComponent', () => {
  let component: RiskOriginComponent;
  let fixture: ComponentFixture<RiskOriginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskOriginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskOriginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
