import { Component, OnInit } from '@angular/core';
import {RiskCategory} from '../model/riskcategory.model';
import {RiskCategoryService} from '../service/riskcategory.service';
import {RiskOriginCategory} from '../model/riskorigincategory.model';
import {RiskOriginCategoryService} from '../service/riskorigincategory.sevice';

@Component({
  selector: 'app-risk-origin',
  templateUrl: './risk-origin.component.html',
  styleUrls: ['./risk-origin.component.css']
})
export class RiskOriginComponent implements OnInit {
  riskOriginCategories: RiskOriginCategory[];
  constructor(private service: RiskOriginCategoryService) {
    this.riskOriginCategories = service.getRiskOriginCategories();
  }

  get totalSize():number {
    return this.riskOriginCategories.reduce((tv, c2) =>  tv + c2.categorySize, 0);
  }

  ngOnInit() {
  }

}
